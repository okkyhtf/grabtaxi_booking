class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.belongs_to :passenger
      t.datetime :booking_time
      t.datetime :pickup_time
      t.decimal{10,6} :pickup_latitude
      t.decimal{10,6} :pickup_longitude
      t.decimal{10,6} :dropoff_latitude
      t.decimal{10,6} :dropoff_longitude
      t.integer :estimated_fare
      t.float :estimated_distance
      t.string :promo_code
      t.integer :driver_tip
      t.string :driver_name
      t.string :driver_phone

      t.timestamps
    end
  end
end
