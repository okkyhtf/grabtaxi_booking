class CreatePassengers < ActiveRecord::Migration
  def change
    create_table :passengers do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :password_hash

      t.timestamps
    end
  end
end
