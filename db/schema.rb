# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140529210408) do

  create_table "bookings", force: true do |t|
    t.integer  "passenger_id"
    t.datetime "booking_time"
    t.datetime "pickup_time"
    t.decimal  "pickup_latitude",    precision: 10, scale: 6
    t.decimal  "pickup_longitude",   precision: 10, scale: 6
    t.decimal  "dropoff_latitude",   precision: 10, scale: 6
    t.decimal  "dropoff_longitude",  precision: 10, scale: 6
    t.integer  "estimated_fare"
    t.float    "estimated_distance"
    t.string   "promo_code"
    t.integer  "driver_tip"
    t.string   "driver_name"
    t.string   "driver_phone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "passengers", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "password_hash"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
