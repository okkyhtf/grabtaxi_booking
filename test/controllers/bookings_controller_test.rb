require 'test_helper'

class BookingsControllerTest < ActionController::TestCase
  setup do
    @booking = bookings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bookings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create booking" do
    assert_difference('Booking.count') do
      post :create, booking: { booking_time: @booking.booking_time, driver_name: @booking.driver_name, driver_phone: @booking.driver_phone, driver_tip: @booking.driver_tip, dropoff_latitude: @booking.dropoff_latitude, dropoff_longitude: @booking.dropoff_longitude, estimated_distance: @booking.estimated_distance, estimated_fare: @booking.estimated_fare, pickup_latitude: @booking.pickup_latitude, pickup_longitude: @booking.pickup_longitude, pickup_time: @booking.pickup_time, promo_code: @booking.promo_code }
    end

    assert_redirected_to booking_path(assigns(:booking))
  end

  test "should show booking" do
    get :show, id: @booking
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @booking
    assert_response :success
  end

  test "should update booking" do
    patch :update, id: @booking, booking: { booking_time: @booking.booking_time, driver_name: @booking.driver_name, driver_phone: @booking.driver_phone, driver_tip: @booking.driver_tip, dropoff_latitude: @booking.dropoff_latitude, dropoff_longitude: @booking.dropoff_longitude, estimated_distance: @booking.estimated_distance, estimated_fare: @booking.estimated_fare, pickup_latitude: @booking.pickup_latitude, pickup_longitude: @booking.pickup_longitude, pickup_time: @booking.pickup_time, promo_code: @booking.promo_code }
    assert_redirected_to booking_path(assigns(:booking))
  end

  test "should destroy booking" do
    assert_difference('Booking.count', -1) do
      delete :destroy, id: @booking
    end

    assert_redirected_to bookings_path
  end
end
