class Api::PassengersController < ApplicationController
  http_basic_authenticate_with name: "nostra", password: "welcome1"
  before_action :fetch_passenger, except: [:index, :create]
  skip_before_action :verify_authenticity_token
  
  def fetch_passenger
    @passenger = Passenger.find(params[:id])
  end
  
  def index
    @passengers = Passenger.all
    respond_to do |format|
      format.json {render json: @passengers}
    end
  end

  def show
    respond_to do |format|
      format.json {render json: @passenger}
    end
  end

  def create
    @passenger = Passenger.new(passenger_params)
    generated_password = @passenger.generate_password
    response = {name: @passenger.name, email: @passenger.email, phone: @passenger.phone, generated_password: generated_password}
    respond_to do |format|
      if @passenger.save
        format.json {render json: response, status: :created}
        puts "\n\nNOTIFICATION: An email is sent to #{@passenger.email} with the generated password #{generated_password}\n\n"
      else
        format.json {render json: @passenger.errors, status: :unprocessable_entity}
      end
    end
  end

  def update
    respond_to do |format|
      if @passenger.update_attributes(passenger_params)
        format.json {head :no_content, status: :ok}
      else
        format.json {render json: @passenger.errors, status: :unprocessable_entity}
      end
    end
  end

  def destroy
    respond_to do |format|
      if @passenger.destroy
        format.json {head :no_content, status: :ok}
      else
        format.json {render json: @passenger.errors, status: :unprocessable_entity}
      end
    end
  end
  
  private
  
  def passenger_params
    params.require(:passenger).permit(:name, :email, :phone, :password_hash) if params[:passenger]
  end
end
