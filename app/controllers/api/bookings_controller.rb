class Api::BookingsController < ApplicationController
  http_basic_authenticate_with name: "nostra", password: "welcome1"
  before_action :fetch_booking, except: [:index, :create]
  skip_before_action :verify_authenticity_token
  
  def fetch_booking
    @booking = Booking.find(params[:id])
  end
  
  def index
    @bookings = Booking.all
    respond_to do |format|
      format.json {render json: @bookings}
    end
  end

  def show
    respond_to do |format|
      format.json {render json: @booking}
    end
  end
  
  def create
    @passenger = Passenger.where(phone: params[:passenger_phone]).first
    if @passenger != nil && @passenger.is_authenticated?(params[:passenger_password])
      @booking = Booking.new(booking_params)
      @booking.passenger = @passenger
      respond_to do |format|
        if @booking.save 
          DriverAssignmentWorker.perform_async(@booking.id)
          format.json {render json: @booking, status: :created}
        else
          format.json {render json: @booking.errors, status: :unprocessable_entity}
        end
      end
    else
      response = {status: "ERROR: Wrong phone number or password!"}
      respond_to do |format|
        format.json {render json: response}
      end
    end
  end
  
  def update
    respond_to do |format|
      if @booking.update_attributes(booking_params)
        format.json {head :no_content, status: :ok}
      else
        format.json {render json: @booking.errors, status: :unprocessable_entity}
      end
    end
  end

  def destroy
    respond_to do |format|
      if @booking.destroy
        format.json {head :no_content, status: :ok}
      else
        format.json {render json: @booking.errors, status: :unprocessable_entity}
      end
    end
  end
  
  private
  
  def booking_params
    params.require(:booking).permit(:booking_time, :pickup_time, :pickup_latitude, :pickup_longitude, :dropoff_latitude, :dropoff_longitude, :estimated_fare, :estimated_distance, :promo_code, :driver_tip, :driver_name, :driver_phone) if params[:booking]
  end
end
