json.array!(@bookings) do |booking|
  json.extract! booking, :id, :booking_time, :pickup_time, :pickup_latitude, :pickup_longitude, :dropoff_latitude, :dropoff_longitude, :estimated_fare, :estimated_distance, :promo_code, :driver_tip, :driver_name, :driver_phone
  json.url booking_url(booking, format: :json)
end
