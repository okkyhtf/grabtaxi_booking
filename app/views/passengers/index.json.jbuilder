json.array!(@passengers) do |passenger|
  json.extract! passenger, :id, :name, :email, :phone, :password_hash
  json.url passenger_url(passenger, format: :json)
end
