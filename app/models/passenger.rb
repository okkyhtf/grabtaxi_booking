class Passenger < ActiveRecord::Base
  validates :name, :email, :phone, presence: true
  validates :email, :phone, uniqueness: true
  
  has_many :bookings
  
  def generate_password
    generated_password = Digest::SHA1.hexdigest(self.phone)
    self.password_hash = Digest::SHA1.hexdigest(generated_password)
    return generated_password
  end
  
  def is_authenticated?(passenger_password)
    return self.password_hash == (Digest::SHA1.hexdigest(passenger_password))
  end
end
