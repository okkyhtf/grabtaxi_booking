class DriverAssignmentWorker
  include Sidekiq::Worker
  require 'rest_client'
  
  sidekiq_options queue: "booking"
  
  DRIVERS_REST_API_BASE_URL = 'http://localhost:3001/api'
  DRIVERS_REST_API_USERNAME = 'nostra'
  DRIVERS_REST_API_PASSWORD = 'welcome1'

  def perform(booking_id)
    uri = "#{DRIVERS_REST_API_BASE_URL}/drivers/assign_to_booking/#{booking_id}.json"
    rest_resource = RestClient::Resource.new(uri, DRIVERS_REST_API_USERNAME, DRIVERS_REST_API_PASSWORD)
    begin
      rest_resource.get
      puts "\n\nDriverAssignmentWorker: Assign Driver ReST API Call Success for Booking ID #{booking_id}!\n\n"
    rescue Exception => e
      puts "\n\nDriverAssignmentWorker: Assign Driver ReST API Call Failed for Booking ID #{booking_id}!\n\n"
      puts "#{e}"
    end
  end
end
